package fabrica_de_software1;

import static org.junit.Assert.assertEquals;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import io.github.bonigarcia.wdm.WebDriverManager;



public class Teste {
	

	
	
	@BeforeClass
	public static void preparandoTeste() {
	
	
	}
	
		
	
	
	@AfterClass
	public static void finalizandoTeste() {
	}
	
	
	@Test
	public void fluxoFeliz() {
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");
		
		WebElement userNameBox = driver.findElement(By.name("username"));
		WebElement passwordBox = driver.findElement(By.name("password"));
		WebElement commentsBox = driver.findElement(By.name("comments"));
		WebElement checkbox1 = driver.findElement(By.xpath("//input[@value='cb1']"));
		WebElement checkbox2 = driver.findElement(By.xpath("//input[@value='cb2']"));
		WebElement checkbox3 = driver.findElement(By.xpath("//input[@value='cb3']"));
		WebElement multipleSelect = driver.findElement(By.name("multipleselect[]"));
	    WebElement dropdown = driver.findElement(By.name("dropdown"));
		WebElement radio1 = driver.findElement(By.xpath("//input[@value='rd1']"));
		WebElement buttonSubmit = driver.findElement(By.xpath("//input[@value='submit']"));

		
		Select selectDropdown = new Select(dropdown);
		Select selectMultipleSelect = new Select(multipleSelect);

		
		
		userNameBox.sendKeys("QA User");
		passwordBox.sendKeys("QA Password");
		commentsBox.clear();
		commentsBox.sendKeys("Comentario");
		checkbox1.click();
		checkbox2.click();
		radio1.click();
		dropdown.click();
		selectDropdown.selectByValue("dd1");
		selectMultipleSelect.selectByValue("ms1");
		selectMultipleSelect.deselectByValue("ms4");
		
		
		buttonSubmit.click();
		
		
		Assert.assertEquals("QA User", driver.findElement(By.id("_valueusername")).getText());
		Assert.assertEquals("QA Password", driver.findElement(By.id("_valuepassword")).getText());
		Assert.assertEquals("Comentario", driver.findElement(By.id("_valuecomments")).getText());
        Assert.assertEquals("cb1", driver.findElement(By.id("_valuecheckboxes0")).getText());
        Assert.assertEquals("cb2", driver.findElement(By.id("_valuecheckboxes1")).getText());
        Assert.assertEquals("cb3", driver.findElement(By.id("_valuecheckboxes2")).getText());
        Assert.assertEquals("rd1", driver.findElement(By.id("_valueradioval")).getText());
        Assert.assertEquals("ms1", driver.findElement(By.id("_valuemultipleselect0")).getText());
        Assert.assertEquals("dd1", driver.findElement(By.id("_valuedropdown")).getText());
        Assert.assertEquals("submit", driver.findElement(By.id("_valuesubmitbutton")).getText());

        
        driver.quit();
	}
        
        @Test
    	public void fluxoAlternativo() {
    		
    		WebDriverManager.chromedriver().setup();
    		WebDriver driver = new ChromeDriver();
    		driver.get("https://testpages.herokuapp.com/styled/basic-html-form-test.html");
    		
    		
    		WebElement userNameBox = driver.findElement(By.name("username"));
    		WebElement passwordBox = driver.findElement(By.name("password"));
    		WebElement commentsBox = driver.findElement(By.name("comments"));
      		WebElement multipleSelect = driver.findElement(By.name("multipleselect[]"));
    	    WebElement dropdown = driver.findElement(By.name("dropdown"));
    		WebElement radio1 = driver.findElement(By.xpath("//input[@value='rd1']"));
    		WebElement buttonSubmit = driver.findElement(By.xpath("//input[@value='submit']"));

    		
    		Select selectDropdown = new Select(dropdown);
    		Select selectMultipleSelect = new Select(multipleSelect);

    		
   
    		commentsBox.clear();
    	 	radio1.click();
    		dropdown.click();
    		selectDropdown.selectByValue("dd1");
    		selectMultipleSelect.selectByValue("ms1");
    		selectMultipleSelect.deselectByValue("ms4");
    		buttonSubmit.click();
    		
    		
    		Assert.assertEquals("No Value for username", driver.findElement(By.xpath("//body/div/div[3]/p[1]/strong")).getText());
    		Assert.assertEquals("No Value for password", driver.findElement(By.xpath("/html/body/div/div[3]/p[2]/strong")).getText());
    		Assert.assertEquals("No Value for comments", driver.findElement(By.xpath("/html/body/div/div[3]/p[3]/strong")).getText());
            Assert.assertEquals("cb3", driver.findElement(By.id("_valuecheckboxes0")).getText());
            Assert.assertEquals("rd1", driver.findElement(By.id("_valueradioval")).getText());
            Assert.assertEquals("ms1", driver.findElement(By.id("_valuemultipleselect0")).getText());
            Assert.assertEquals("dd1", driver.findElement(By.id("_valuedropdown")).getText());
            Assert.assertEquals("submit", driver.findElement(By.id("_valuesubmitbutton")).getText());
            
            
            
            driver.quit();
            

	}

}
